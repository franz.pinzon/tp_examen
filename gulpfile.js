var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var autoprefix = require('gulp-autoprefixer');
var csso = require('gulp-csso');
var browserSync = require('browser-sync').create();
var uncss = require('gulp-uncss');



gulp.task('style', function(){
	return gulp.src('src/**/*/*.scss')
	.pipe(sourcemaps.init())
		.pipe(sass())
		.pipe(uncss({
	            html: ['src/index.html']
	        }))
		.pipe(autoprefix({
			browsers: ['last 2 versions']
		}))
		.pipe(csso())

	.pipe(sourcemaps.write())
	.pipe(gulp.dest('.tmp'))
	.pipe(gulp.dest('dist'))

});


gulp.task('default', ['style'], function(){
	return gulp.src('src/**/*.html')
	.pipe(gulp.dest('dist'))

});